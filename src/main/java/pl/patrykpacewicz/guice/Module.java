package pl.patrykpacewicz.guice;

import com.google.inject.AbstractModule;
import pl.patrykpacewicz.hello.rest.HelloWorldResource;

public class Module extends AbstractModule {
    @Override
    protected void configure() {
        bind(HelloWorldResource.class);
    }
}
