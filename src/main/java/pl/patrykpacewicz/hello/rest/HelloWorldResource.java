package pl.patrykpacewicz.hello.rest;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import pl.patrykpacewicz.hello.model.HelloModel;

@Path("/")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class HelloWorldResource {
    private final HelloModel helloModel;

    @Inject
    public HelloWorldResource(HelloModel helloModel) {
        this.helloModel = helloModel;
    }

    @GET
    public Response getHelloWorld() {
        return Response.ok().entity(helloModel).build();
    }
}
