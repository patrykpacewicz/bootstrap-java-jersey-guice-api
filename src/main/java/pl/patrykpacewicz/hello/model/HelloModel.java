package pl.patrykpacewicz.hello.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HelloModel {
    @XmlElement private final String hello;
    @XmlElement private final String world;

    public HelloModel(String hello, String world) {
        this.hello = hello;
        this.world = world;
    }

    public HelloModel() {
        this("Hello", "World");
    }

    public String getHello() {
        return hello;
    }

    public String getWorld() {
        return world;
    }
}
