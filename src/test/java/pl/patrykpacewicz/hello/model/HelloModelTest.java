package pl.patrykpacewicz.hello.model;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;


public class HelloModelTest {
    @Test
    public void shouldReturnDefaultValuesOnEmptyConstructor() {
        HelloModel helloModel = new HelloModel();

        assertThat(helloModel.getHello()).isEqualTo("Hello");
        assertThat(helloModel.getWorld()).isEqualTo("World");
    }

    @Test
    public void shoudlReturnValuesFromContructor() throws Exception {
        HelloModel helloModel = new HelloModel("param1", "param2");

        assertThat(helloModel.getHello()).isEqualTo("param1");
        assertThat(helloModel.getWorld()).isEqualTo("param2");
    }
}
