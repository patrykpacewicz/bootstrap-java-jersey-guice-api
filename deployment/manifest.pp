import 'config.pp'

file { "${cfg_rootDir}/gradle.properties":
    ensure  => file,
    content => template("${cfg_templatesDir}/gradle.properties"),
}

import 'manifests/*.pp'