include wget

wget::fetch { "jetty-download":
    source      => "http://eclipse.org/downloads/download.php?file=/jetty/stable-9/dist/jetty-distribution-${cfg_jetty_version}.tar.gz&r=1",
    destination => "/tmp/jetty-distribution-${cfg_jetty_version}.tar.gz",
}

exec { "jetty-untar":
	command => "tar xf /tmp/jetty-distribution-${cfg_jetty_version}.tar.gz",
	cwd     => "/opt",
	creates => "/opt/jetty-distribution-${cfg_jetty_version}",
	path    => ["/bin"],
	notify  => Service["jetty"],
	require => [ Wget::Fetch['jetty-download'], Package['oracle-java8-installer'] ],
}

file { "${cfg_jetty_dir}/":
    ensure  => "/opt/jetty-distribution-${cfg_jetty_version}",
    require => Exec['jetty-untar'],
}

file { "${cfg_jetty_logDir}":
	ensure  => "${cfg_jetty_dir}/logs",
	require => Exec['jetty-untar'],
}

file { "/etc/init.d/jetty":
	ensure  => "${$cfg_jetty_dir}/bin/jetty.sh",
	require => Exec['jetty-untar'],
}

file { "${cfg_jetty_dir}/etc/realm.properties":
    ensure  => file,
    content => template("${cfg_templatesDir}/jetty-realm.properties"),
	require => [
		File["${cfg_jetty_dir}/"],
		File["${cfg_jetty_logDir}"],
		File['/etc/init.d/jetty'],
	],
    notify  => Service['jetty'],
}

file { "/etc/default/jetty":
    ensure  => file,
    content => template("${cfg_templatesDir}/jetty-default"),
    require => File['/etc/init.d/jetty'],
    notify  => Service['jetty'],
}

file { "${cfg_jetty_dir}/webapps/cargo-jetty-7-with-basic-auth-1.4.4.war":
    mode    => 664,
    owner   => root,
    group   => root,
    source  => "${$cfg_packagesDir}/cargo-jetty-7-with-basic-auth-1.4.4.war",
    notify  => Service['jetty'],
}

file { "${cfg_jetty_dir}/webapps.demo":
	ensure  => 'absent',
	purge   => true,
	recurse => true,
	force   => true,
    notify  => Service['jetty'],
}

service {"jetty":
    enable     => true,
    ensure     => running,
    hasstatus  => false,
    hasrestart => true,
}
