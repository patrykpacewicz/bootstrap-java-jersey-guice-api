$cfg_codeName     = $::appname
$cfg_debug        = $::appdebug
$cfg_rootDir      = $::appdir
$cfg_templatesDir = "${::appdir}/deployment/templates"
$cfg_packagesDir  = "${::appdir}/deployment/packages"
$cfg_logDir       = "/var/log/${cfg_codeName}"

$cfg_jetty_version   = '9.0.6.v20130930'
$cfg_jetty_dir       = '/opt/jetty'
$cfg_jetty_logDir    = '/var/log/jetty'

$cfg_jetty_hostname  = 'localhost'
$cfg_jetty_port      = $::jettyport
$cfg_jetty_password  = $::jettypassword
$cfg_jetty_username  = 'cargo'
