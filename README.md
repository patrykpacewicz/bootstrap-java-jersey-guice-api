[![Build Status](https://drone.io/bitbucket.org/patrykpacewicz/bootstrap-java-jersey-guice-api/status.png)](https://drone.io/bitbucket.org/patrykpacewicz/bootstrap-java-jersey-guice-api/latest)

Bootstrap - Java - Jersey - Guice
=================================
Code is prepared so you can easily start working on a new applications.
It has production and development configuration.

Launch for development
----------------------
Application is connected to the vagrant project,
which will prepare the virtual machine and configure it for proper working.
```
vagrant up
```
Enjoy [http://localhost:8882/](http://localhost:8882/)  :-)

Launch on production environment
--------------------------------
Application is also prepared for easy production deployment
```
git clone https://bitbucket.org/patrykpacewicz/wmid-api-security.git /app/backend
cd /app/backend
deployment/bootstrap.sh
```

What's inside ?
---------------
 * Jersey + Guice
 * ./gradlew (build, war, jettyRun, test, check)
 * Puppet for server configuration
 * Example: 'Hello World!'
